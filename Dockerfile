FROM node AS build

COPY . /app
#premier point pour le dossier courant, gatsby, le deuxième pour le dossier
#courant dans l'image, /app

WORKDIR /app
# Workdir créé un dossier /app et nous positionne dedans (dans l'image)

RUN npm install -g gatsby-cli
#installe gatsby en global

RUN npm i
#installe les dépendances

#ENTRYPOINT [ "gatsby", "develop", "-H", "0.0.0.0" ]
# "-H", "0.0.0.0" ajoutent un masque qui permet l'acces à gatsby develop

RUN gatsby build

#appel:
#   - docker build -t gatsby .
#   - docker run -p 5001:8000 gatsby
# sur le net: localhost:5001

FROM nginx
WORKDIR /usr/share/nginx/html
COPY --from=build /app/public .
ENTRYPOINT ["nginx", "-g", "daemon off;"]
#   - docker build -t gatsby .
#   - docker run -p 5002:80 gatsby
# sur le net: localhost:5002